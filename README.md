##Instruções

- Instalar node e npm
```sh
apt install nodejs npm
```

- Instalar o angular cli
```sh
sudo npm install -g @angular/cli
```

- Instalar as dependências
```sh
npm install
```

- Rodar serve
```sh
ng serve
```

- Se tudo der certo a aplicação estará rodando em  `http://localhost:4200/`