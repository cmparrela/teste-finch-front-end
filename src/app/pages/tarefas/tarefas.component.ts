import { Component, OnInit } from '@angular/core';
import { TarefaService } from 'src/app/services/tarefa.service';
import { Tarefa } from 'src/app/models/tarefa.model';

@Component({
    selector: 'app-tarefas',
    templateUrl: './tarefas.component.html',
    styleUrls: ['./tarefas.component.css']
})
export class TarefasComponent implements OnInit {
    tarefas: Array<Tarefa>

    constructor(public tarefaService: TarefaService) { }

    ngOnInit() {
        this.index();
    }

    index(){
        this.tarefaService.lista().then((dados:any)=>{
            this.tarefas = dados.retorno;
        })
    }

    deleta(tarefa: Tarefa){
        this.tarefaService.deleta(tarefa.codigo).then((dados:any)=>{
            if (dados.status) {
                this.index();
            }
        })
    }

}
