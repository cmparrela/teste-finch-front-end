import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaAdicionarComponent } from './tarefa-adicionar.component';

describe('TarefaAdicionarComponent', () => {
    let component: TarefaAdicionarComponent;
    let fixture: ComponentFixture<TarefaAdicionarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TarefaAdicionarComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TarefaAdicionarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
