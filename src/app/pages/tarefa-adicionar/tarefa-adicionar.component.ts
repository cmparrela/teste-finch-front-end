import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TarefaService } from 'src/app/services/tarefa.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-tarefa-adicionar',
    templateUrl: './tarefa-adicionar.component.html',
    styleUrls: ['./tarefa-adicionar.component.css']
})
export class TarefaAdicionarComponent implements OnInit {
    formTarefa: FormGroup;
    tentativaEnvio = false;

    constructor(
        public formBuilder: FormBuilder,
        public tarefaService: TarefaService,
        public router: Router,
    ) { }

    ngOnInit() {
        this.formTarefa = this.formBuilder.group({
            titulo: ['', [Validators.required]],
            descricao: [''],
            situacao: ['', [Validators.required]],
        });
    }

    envia() {
        this.tentativaEnvio = true;

        if (this.formTarefa.valid) {
            this.tarefaService.cria(this.formTarefa.value).then((dados: any) => {
                if (dados.status) {
                    this.router.navigateByUrl('/');
                }
            })
        }
    }

}
