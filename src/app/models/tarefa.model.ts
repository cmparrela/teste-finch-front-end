export class Tarefa {
    codigo: number;
    titulo: string;
    descricao: string;
    situacao: number;
  }