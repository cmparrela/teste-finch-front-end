import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TarefaAdicionarComponent } from './pages/tarefa-adicionar/tarefa-adicionar.component';
import { TarefaEditarComponent } from './pages/tarefa-editar/tarefa-editar.component';
import { TarefasComponent } from './pages/tarefas/tarefas.component';

const routes: Routes = [
  {
    path: 'tarefas',
    component: TarefasComponent,
    data: { title: 'Lista de tarefa' }
  },
  {
    path: 'tarefa-adicionar',
    component: TarefaAdicionarComponent,
    data: { title: 'Adicionar tarefa' }
  },
  {
    path: 'tarefa-editar/:id',
    component: TarefaEditarComponent,
    data: { title: 'Editar tarefa' }
  },
  {
    path: '',
    redirectTo: '/tarefas',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
