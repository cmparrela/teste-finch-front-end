import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Tarefa } from '../models/tarefa.model';

@Injectable({
    providedIn: 'root'
})
export class TarefaService {
    httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    apiUrl = "http://localhost:8000/api/tarefa";

    constructor(
        private http: HttpClient
    ) { }

    lista() {
        return new Promise((resolve, reject) => {
            this.http.get(this.apiUrl, this.httpOptions)
                .subscribe(data => {
                    resolve(data)
                }, (error) => {
                    reject(error);
                });
        });
    }

    cria(dados: Tarefa) {
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl, dados, this.httpOptions)
                .subscribe(data => {
                    resolve(data)
                }, (error) => {
                    reject(error);
                });
        });
    }

    edita() { }

    deleta(codigo) {
        return new Promise((resolve, reject) => {
            this.http.delete(`${this.apiUrl}/${codigo}`, this.httpOptions)
                .subscribe(data => {
                    console.log(data);
                    resolve(data)
                }, (error) => {
                    reject(error);
                });
        });
    }
}
